User Profiling System
==================

# Intro 

The User Profiling System is able to automatically generate a profile of the user, while considering their browsing patterns. The profile will indicate the interests of the user in each of the categories defined by the IAB.

![image info](./UserProfilingDesign.png)


# Installation 

We recommend you to use the python code under the folder "profilingAlgorithm". You can directly copy the folder and start using it. We also provide an REST server as reference on how you can integrate the User Profiling System into a platform like EasyPIMS.

# Dependencies

•	SQLAlchemy 		1.4.1
•	tornado	 	6.1
•	tornado-sqlalchemy	0.7.0
•	gensim 		3.8.3


# Usage Example

The profiling algorithm uses the standard scikit-learn API

```python
    def __init__(self, name, infer=True):
        """Return a SeqModel object whose name is *name* and which is either used for training or inference."""
        self.name = name
        self.infer = infer

    def load(self, path):
        """Loads a pretrained model from a file."""

    def train(self, xdata, ydata):
        """Trains the model from scratch. Set params in separate method."""

    def update(self, tseq, target):
        """Update the model (only possible if online SGD training possible)."""

    def update_batch(self, xdata, ydata):
        """Update the model with a batch of sequences."""

    def predict(self, seqData):
        """Apply the predict method (or equivalent) of the model and return directly"""

    def get_predict(self):
        """Return the value of the last predict call."""

    def predict_probab(self, seqData):
        """Apply the predict_probab method (or equivalent) of the model and return directly"""

    def get_predict_probab(self):
        """Return the value of the last predict_probab call."""

    def set_params(self, params):
        """Set the parameters of the internal machine learning model."""
```
## Train the model

```python
from profilingAlgorithm.models.SequenceModel import SeqModelGensimWord2Vec
myModel = SeqModelGensimWord2Vec("PreviousDay_model", infer=False)
#sequences should ve a list of lists. In each list we should have the list of hosts visited by each user in order.
sequences = loadSequences()
myModel.train(sequences, None)
myModel.save("./models")
```

## Inference

```python
from profilingAlgorithm.models.SequenceModel import SeqModelGensimWord2Vec
myModel = SeqModelGensimWord2Vec("PreviousDay_model", infer=False)
myModel.load('./models')

# host_cats_vector is a dict that includes the assigned categories for the known hostnames in the form of a vector.
# host_cats_vectors = {"google.com": [1, 0, 0, 0, 0, 0, 0],
#                      "bbc.co.uk": [1, 0, 0, 0, 1, 0.5, 0.5],
#                      "nyt.com": [0, 0, 0, 0.3, 0, 0, 0],
#                      "espn.com": [0, 1, 0, 0, 0, 0, 0],
#                      "realmadrid.com": [1, 0, 1, 0, 0, 0, 0],
#                      }
myModelset_domainToCategories(host_cats_vectors)

# listURLs is a list with the hostnames we want to obtain a profile for (order is not important, repetition is not important.)
# normalizePrediction="Norm" returns a vector of categories normalized (it sums 1)
# normalizePrediction="Max1" returns a vector of categories whith values between 0 and 1 that is not normalized.
predict_probab_array = net2vec.predict_probab(listURLs,  normalizePrediction="Norm")

```







# License

The User Profiling System is released under the NLE’s academic or non-profit organization non-commercial research only, see the `LICENSE` file.


  Copyright (C) 2022  NEC Laboratories Europe - Roberto González, David Friede, Mathias Niepert
