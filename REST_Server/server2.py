##
# User Profiling System
#
# File:     Server2.py
# Authors:  <Roberto Gonzalez (roberto.gonzalez@neclab.eu)>
# <Mathias Niepert (mathias.niepert@neclab.eu)>
# <David Friede (david.friede@neclab.eu)>
#
# NEC Laboratories Europe GmbH, Copyright (c) <year>, All rights reserved.
#
# THIS HEADER MAY NOT BE EXTRACTED OR MODIFIED IN ANY WAY.
#
# PROPRIETARY INFORMATION ---
#
# SOFTWARE LICENSE AGREEMENT
#
# ACADEMIC OR NON-PROFIT ORGANIZATION NONCOMMERCIAL RESEARCH USE ONLY
#
# BY USING OR DOWNLOADING THE SOFTWARE, YOU ARE AGREEING TO THE TERMS OF THIS
# LICENSE AGREEMENT.  IF YOU DO NOT AGREE WITH THESE TERMS, YOU MAY NOT USE OR
# DOWNLOAD THE SOFTWARE.
#
# This is a license agreement ("Agreement") between your academic institution
# or non-profit organization or self (called "Licensee" or "You" in this
# Agreement) and NEC Laboratories Europe GmbH (called "Licensor" in this
# Agreement).  All rights not specifically granted to you in this Agreement
# are reserved for Licensor.
#
# RESERVATION OF OWNERSHIP AND GRANT OF LICENSE: Licensor retains exclusive
# ownership of any copy of the Software (as defined below) licensed under this
# Agreement and hereby grants to Licensee a personal, non-exclusive,
# non-transferable license to use the Software for noncommercial research
#     purposes, without the right to sublicense, pursuant to the terms and
# conditions of this Agreement. NO EXPRESS OR IMPLIED LICENSES TO ANY OF
# LICENSOR'S PATENT RIGHTS ARE GRANTED BY THIS LICENSE. As used in this
# Agreement, the term "Software" means (i) the actual copy of all or any
# portion of code for program routines made accessible to Licensee by Licensor
# pursuant to this Agreement, inclusive of backups, updates, and/or merged
# copies permitted hereunder or subsequently supplied by Licensor,  including
# all or any file structures, programming instructions, user interfaces and
# screen formats and sequences as well as any and all documentation and
# instructions related to it, and (ii) all or any derivatives and/or
# modifications created or made by You to any of the items specified in (i).
#
# CONFIDENTIALITY/PUBLICATIONS: Licensee acknowledges that the Software is
# proprietary to Licensor, and as such, Licensee agrees to receive all such
# materials and to use the Software only in accordance with the terms of this
# Agreement.  Licensee agrees to use reasonable effort to protect the Software
# from unauthorized use, reproduction, distribution, or publication. All
# publication materials mentioning features or use of this software must
# explicitly include an acknowledgement the software was developed by NEC
# Laboratories Europe GmbH.
#
# COPYRIGHT: The Software is owned by Licensor.
#
# PERMITTED USES:  The Software may be used for your own noncommercial
# internal research purposes. You understand and agree that Licensor is not
# obligated to implement any suggestions and/or feedback you might provide
# regarding the Software, but to the extent Licensor does so, you are not
# entitled to any compensation related thereto.
#
# DERIVATIVES: You may create derivatives of or make modifications to the
# Software, however, You agree that all and any such derivatives and
# modifications will be owned by Licensor and become a part of the Software
# licensed to You under this Agreement.  You may only use such derivatives and
# modifications for your own noncommercial internal research purposes, and you
# may not otherwise use, distribute or copy such derivatives and modifications
# in violation of this Agreement.
#
# BACKUPS:  If Licensee is an organization, it may make that number of copies
# of the Software necessary for internal noncommercial use at a single site
# within its organization provided that all information appearing in or on the
# original labels, including the copyright and trademark notices are copied
# onto the labels of the copies.
#
# USES NOT PERMITTED:  You may not distribute, copy or use the Software except
# as explicitly permitted herein. Licensee has not been granted any trademark
# license as part of this Agreement.  Neither the name of NEC Laboratories
# Europe GmbH nor the names of its contributors may be used to endorse or
# promote products derived from this Software without specific prior written
# permission.
#
# You may not sell, rent, lease, sublicense, lend, time-share or transfer, in
# whole or in part, or provide third parties access to prior or present
# versions (or any parts thereof) of the Software.
#
# ASSIGNMENT: You may not assign this Agreement or your rights hereunder
# without the prior written consent of Licensor. Any attempted assignment
# without such consent shall be null and void.
#
# TERM: The term of the license granted by this Agreement is from Licensee's
# acceptance of this Agreement by downloading the Software or by using the
# Software until terminated as provided below.
#
# The Agreement automatically terminates without notice if you fail to comply
# with any provision of this Agreement.  Licensee may terminate this Agreement
# by ceasing using the Software.  Upon any termination of this Agreement,
# Licensee will delete any and all copies of the Software. You agree that all
# provisions which operate to protect the proprietary rights of Licensor shall
# remain in force should breach occur and that the obligation of
# confidentiality described in this Agreement is binding in perpetuity and, as
# such, survives the term of the Agreement.
#
# FEE: Provided Licensee abides completely by the terms and conditions of this
# Agreement, there is no fee due to Licensor for Licensee's use of the
#     Software in accordance with this Agreement.
#
# DISCLAIMER OF WARRANTIES:  THE SOFTWARE IS PROVIDED "AS-IS" WITHOUT WARRANTY
# OF ANY KIND INCLUDING ANY WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR
# FITNESS FOR A PARTICULAR USE OR PURPOSE OR OF NON- INFRINGEMENT.  LICENSEE
# BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF THE SOFTWARE AND
# RELATED MATERIALS.
#
# SUPPORT AND MAINTENANCE: No Software support or training by the Licensor is
# provided as part of this Agreement.
#
# EXCLUSIVE REMEDY AND LIMITATION OF LIABILITY: To the maximum extent
# permitted under applicable law, Licensor shall not be liable for direct,
#     indirect, special, incidental, or consequential damages or lost profits
# related to Licensee's use of and/or inability to use the Software, even if
# Licensor is advised of the possibility of such damage.
#
# EXPORT REGULATION: Licensee agrees to comply with any and all applicable
# export control laws, regulations, and/or other laws related to embargoes and
# sanction programs administered by law.
#
# SEVERABILITY: If any provision(s) of this Agreement shall be held to be
# invalid, illegal, or unenforceable by a court or other tribunal of competent
# jurisdiction, the validity, legality and enforceability of the remaining
# provisions shall not in any way be affected or impaired thereby.
#
# NO IMPLIED WAIVERS: No failure or delay by Licensor in enforcing any right
# or remedy under this Agreement shall be construed as a waiver of any future
# or other exercise of such right or remedy by Licensor.
#
# GOVERNING LAW: This Agreement shall be construed and enforced in accordance
# with the laws of Germany without reference to conflict of laws principles.
# You consent to the personal jurisdiction of the courts of this country and
# waive their rights to venue outside of Germany.
#
# ENTIRE AGREEMENT AND AMENDMENTS: This Agreement constitutes the sole and
# entire agreement between Licensee and Licensor as to the matter set forth
# herein and supersedes any previous agreements, understandings, and
# arrangements between the parties relating hereto.
#
# THIS HEADER MAY NOT BE EXTRACTED OR MODIFIED IN ANY WAY.
##


from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado.web import Application
from tornado_sqlalchemy import SQLAlchemy

# from .views import MainHandler, ProfilesHandler, GetProfileHandler, \
#                    CategoriesHandler, IgnoredCategoriesHandler, TrainHandler


define('port', default=8888, help='port to listen on', type=int)
DATABASE_URL = 'sqlite:///database.db'
db = SQLAlchemy(url=DATABASE_URL)


def main():
    engine = db.create_engine()
    db.metadata.create_all(bind=engine)
    app = MyApplication()
    http_server = HTTPServer(app)
    http_server.listen(options.port)
    print(f'listening on http://localhost:{options.port}')
    IOLoop.current().start()


class MyApplication(Application):
    def __init__(self):
        handlers = [
            (f'/', MainHandler),
            (f'/profiles', ProfilesHandler),
            (f'/profile/([0-9]+)', GetProfileHandler),
            (f'/categories/([0-9]+)', CategoriesHandler),
            (f'/ignored/([0-9]+)', IgnoredCategoriesHandler),
            (f'/train/([0-9]+)', TrainHandler)
        ]

        settings = {
            'db': db
        }

        self.handlers = handlers
        self.settings = settings
        super().__init__(handlers, **settings)


# models.py
from sqlalchemy import Column, Integer, String, Float
from tornado_sqlalchemy import SQLAlchemy

DATABASE_URL = 'sqlite:///database.db'
db = SQLAlchemy(url=DATABASE_URL)


class Profile(db.Model):
    __tablename__ = 'profiles'
    id = Column('id', Integer, primary_key=True)
    user_id = Column('user_id', String(255), unique=True)
    host_visited = Column('host_visited', String, default='')
    last_profiled = Column('last_profiled', String, default='')
    ignored_categories = Column('ignored_categories', String, default='')
    cat_0 = Column('cat_0', Float, default=.0)
    cat_1 = Column('cat_1', Float, default=.0)
    cat_2 = Column('cat_2', Float, default=.0)
    cat_3 = Column('cat_3', Float, default=.0)
    cat_4 = Column('cat_4', Float, default=.0)

    def get_dict(self):
        out = {
            'user_id': self.user_id,
            'id': self.id,
            'host_visited': self.host_visited,
            'last_profiled': self.last_profiled,
            'ignored_categories': self.ignored_categories,
            'categories': self.get_categories()
        }
        return {self.user_id: out}

    def get_categories(self):
        cats = {
            'cat_0': self.cat_0,
            'cat_1': self.cat_1,
            'cat_2': self.cat_2,
            'cat_3': self.cat_3,
            'cat_4': self.cat_4
        }
        return cats

    def set_categories(self, cat_dict, ignore=True):
        if ignore:
            for key in self.get_ignored_categories():
                cat_dict.pop(key, None)
        self.cat_0 = cat_dict.get('cat_0', self.cat_0)
        self.cat_1 = cat_dict.get('cat_1', self.cat_1)
        self.cat_2 = cat_dict.get('cat_2', self.cat_2)
        self.cat_3 = cat_dict.get('cat_3', self.cat_3)
        self.cat_4 = cat_dict.get('cat_4', self.cat_4)
        return cat_dict

    def del_categories(self):
        cats = {f'cat_{n}': .0 for n in range(5)}
        self.set_categories(cats, ignore=False)

    def get_ignored_categories(self):
        if len(self.ignored_categories) == 0:
            return list()
        else:
            return self.ignored_categories.split(' ')

    def add_ignored_categories(self, cat_list):
        ignored_old = self.get_ignored_categories()
        ignored_new = list(set(ignored_old + cat_list))
        self.ignored_categories = ' '.join(ignored_new)

    def __repr__(self):
        return f'{self.user_id} ({self.id})  {self.get_categories()}'



# views.py
from datetime import datetime
from tornado.escape import json_decode
from tornado.web import RequestHandler
from tornado_sqlalchemy import SessionMixin
# from .models import Profile
# from .utils import read_hosts_visited, use_net2vec
from utils import read_hosts_visited, use_net2vec


class MainHandler(RequestHandler):
    def initialize(self):
        self.SUPPORTED_METHODS = ('GET')

    def get(self):
        out = {link: handler.__name__ for link, handler in self.application.handlers}
        self.write(out)


class ProfilesHandler(SessionMixin, RequestHandler):
    def initialize(self):
        self.SUPPORTED_METHODS = ('GET')

    def get(self):
        with self.make_session() as session:
            num_profiles = session.query(Profile).count()
            self.write(f'total number of {num_profiles} profiles')


class GetProfileHandler(SessionMixin, RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Content-Type, api_key, Authorization")
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS')

    def initialize(self):
        self.SUPPORTED_METHODS = ('GET', 'DELETE', 'OPTIONS')

    def get(self, user_id):
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID. creating new user')
                profile = Profile(user_id=user_id)
                session.add(profile)
                session.commit()
            else:
                self.write(profile.get_dict())

    def delete(self, user_id):
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID')
            else:
                session.delete(profile)
                session.commit()
                self.write('deleted')

    def options(self, user_id):
        self.set_status(204)
        self.finish()



class CategoriesHandler(SessionMixin, RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Content-Type, api_key, Authorization")
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS')

    def initialize(self):
        self.SUPPORTED_METHODS = ('GET', 'PUT', 'DELETE', 'OPTIONS')

    def get(self, user_id):
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID')
            else:
                self.write(profile.get_categories())

    def put(self, user_id):
        user_categories = json_decode(self.request.body)
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID')
            else:
                profile.set_categories(user_categories, ignore=False)
                profile.add_ignored_categories(list(user_categories.keys()))
                session.commit()
                self.write(profile.get_categories())

    def delete(self, user_id):
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID')
            else:
                profile.del_categories()
                session.commit()
                self.write(profile.get_categories())

    def options(self, user_id):
        self.set_status(204)
        self.finish()


class IgnoredCategoriesHandler(SessionMixin, RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Content-Type, api_key, Authorization")
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS')

    def initialize(self):
        self.SUPPORTED_METHODS = ('GET', 'PUT', 'OPTIONS')

    def get(self, user_id):
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID')
            else:
                out = {'ignored_categories': profile.ignored_categories}
                self.write(out)

    def put(self, user_id):
        user_ignored_categories = json_decode(self.request.body)['ignored_categories']
        print(user_ignored_categories)
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID')
            else:
                profile.ignored_categories = user_ignored_categories
                session.commit()
                out = {'ignored_categories': profile.ignored_categories}
                self.write(out)

    def options(self, user_id):
        self.set_status(204)
        self.finish()


class TrainHandler(SessionMixin, RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Content-Type, api_key, Authorization")
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS')

    def initialize(self):
        self.SUPPORTED_METHODS = ('GET', 'OPTIONS')

    def get(self, user_id):
        with self.make_session() as session:
            profile = session.query(Profile).filter(Profile.user_id==user_id).first()
            if profile is None:
                self.set_status(404)
                self.write('unknown userID')
            else:
                if profile.host_visited == '':
                    self.set_status(401)
                    self.write('no visited hosts')
                else:
                    hv_str = read_hosts_visited(profile.host_visited)
                    hv_json = {'host_visited': hv_str}
                    net2vec_cats = use_net2vec(hv_json)
                    cats_filt = profile.set_categories(net2vec_cats, ignore=True)
                    now = datetime.utcnow().replace(microsecond=0).isoformat()
                    profile.last_profiled = now
                    session.commit()
                    out = {
                        'last_profiled': profile.last_profiled,
                        'ignored_categories': profile.ignored_categories,
                        'categories': profile.get_categories(),
                        'categories_new': cats_filt
                    }
                    self.write(out)

    def options(self, user_id):
        self.set_status(204)
        self.finish()


# server.py
if __name__ == '__main__':
    main()
