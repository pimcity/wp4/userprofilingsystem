##
# User Profiling System
#
# File:     TrafficSimulator.py
# Authors:  <Roberto Gonzalez (roberto.gonzalez@neclab.eu)>
# <Mathias Niepert (mathias.niepert@neclab.eu)>
# <David Friede (david.friede@neclab.eu)>
#
# NEC Laboratories Europe GmbH, Copyright (c) <year>, All rights reserved.
#
# THIS HEADER MAY NOT BE EXTRACTED OR MODIFIED IN ANY WAY.
#
# PROPRIETARY INFORMATION ---
#
# SOFTWARE LICENSE AGREEMENT
#
# ACADEMIC OR NON-PROFIT ORGANIZATION NONCOMMERCIAL RESEARCH USE ONLY
#
# BY USING OR DOWNLOADING THE SOFTWARE, YOU ARE AGREEING TO THE TERMS OF THIS
# LICENSE AGREEMENT.  IF YOU DO NOT AGREE WITH THESE TERMS, YOU MAY NOT USE OR
# DOWNLOAD THE SOFTWARE.
#
# This is a license agreement ("Agreement") between your academic institution
# or non-profit organization or self (called "Licensee" or "You" in this
# Agreement) and NEC Laboratories Europe GmbH (called "Licensor" in this
# Agreement).  All rights not specifically granted to you in this Agreement
# are reserved for Licensor.
#
# RESERVATION OF OWNERSHIP AND GRANT OF LICENSE: Licensor retains exclusive
# ownership of any copy of the Software (as defined below) licensed under this
# Agreement and hereby grants to Licensee a personal, non-exclusive,
# non-transferable license to use the Software for noncommercial research
#     purposes, without the right to sublicense, pursuant to the terms and
# conditions of this Agreement. NO EXPRESS OR IMPLIED LICENSES TO ANY OF
# LICENSOR'S PATENT RIGHTS ARE GRANTED BY THIS LICENSE. As used in this
# Agreement, the term "Software" means (i) the actual copy of all or any
# portion of code for program routines made accessible to Licensee by Licensor
# pursuant to this Agreement, inclusive of backups, updates, and/or merged
# copies permitted hereunder or subsequently supplied by Licensor,  including
# all or any file structures, programming instructions, user interfaces and
# screen formats and sequences as well as any and all documentation and
# instructions related to it, and (ii) all or any derivatives and/or
# modifications created or made by You to any of the items specified in (i).
#
# CONFIDENTIALITY/PUBLICATIONS: Licensee acknowledges that the Software is
# proprietary to Licensor, and as such, Licensee agrees to receive all such
# materials and to use the Software only in accordance with the terms of this
# Agreement.  Licensee agrees to use reasonable effort to protect the Software
# from unauthorized use, reproduction, distribution, or publication. All
# publication materials mentioning features or use of this software must
# explicitly include an acknowledgement the software was developed by NEC
# Laboratories Europe GmbH.
#
# COPYRIGHT: The Software is owned by Licensor.
#
# PERMITTED USES:  The Software may be used for your own noncommercial
# internal research purposes. You understand and agree that Licensor is not
# obligated to implement any suggestions and/or feedback you might provide
# regarding the Software, but to the extent Licensor does so, you are not
# entitled to any compensation related thereto.
#
# DERIVATIVES: You may create derivatives of or make modifications to the
# Software, however, You agree that all and any such derivatives and
# modifications will be owned by Licensor and become a part of the Software
# licensed to You under this Agreement.  You may only use such derivatives and
# modifications for your own noncommercial internal research purposes, and you
# may not otherwise use, distribute or copy such derivatives and modifications
# in violation of this Agreement.
#
# BACKUPS:  If Licensee is an organization, it may make that number of copies
# of the Software necessary for internal noncommercial use at a single site
# within its organization provided that all information appearing in or on the
# original labels, including the copyright and trademark notices are copied
# onto the labels of the copies.
#
# USES NOT PERMITTED:  You may not distribute, copy or use the Software except
# as explicitly permitted herein. Licensee has not been granted any trademark
# license as part of this Agreement.  Neither the name of NEC Laboratories
# Europe GmbH nor the names of its contributors may be used to endorse or
# promote products derived from this Software without specific prior written
# permission.
#
# You may not sell, rent, lease, sublicense, lend, time-share or transfer, in
# whole or in part, or provide third parties access to prior or present
# versions (or any parts thereof) of the Software.
#
# ASSIGNMENT: You may not assign this Agreement or your rights hereunder
# without the prior written consent of Licensor. Any attempted assignment
# without such consent shall be null and void.
#
# TERM: The term of the license granted by this Agreement is from Licensee's
# acceptance of this Agreement by downloading the Software or by using the
# Software until terminated as provided below.
#
# The Agreement automatically terminates without notice if you fail to comply
# with any provision of this Agreement.  Licensee may terminate this Agreement
# by ceasing using the Software.  Upon any termination of this Agreement,
# Licensee will delete any and all copies of the Software. You agree that all
# provisions which operate to protect the proprietary rights of Licensor shall
# remain in force should breach occur and that the obligation of
# confidentiality described in this Agreement is binding in perpetuity and, as
# such, survives the term of the Agreement.
#
# FEE: Provided Licensee abides completely by the terms and conditions of this
# Agreement, there is no fee due to Licensor for Licensee's use of the
#     Software in accordance with this Agreement.
#
# DISCLAIMER OF WARRANTIES:  THE SOFTWARE IS PROVIDED "AS-IS" WITHOUT WARRANTY
# OF ANY KIND INCLUDING ANY WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR
# FITNESS FOR A PARTICULAR USE OR PURPOSE OR OF NON- INFRINGEMENT.  LICENSEE
# BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF THE SOFTWARE AND
# RELATED MATERIALS.
#
# SUPPORT AND MAINTENANCE: No Software support or training by the Licensor is
# provided as part of this Agreement.
#
# EXCLUSIVE REMEDY AND LIMITATION OF LIABILITY: To the maximum extent
# permitted under applicable law, Licensor shall not be liable for direct,
#     indirect, special, incidental, or consequential damages or lost profits
# related to Licensee's use of and/or inability to use the Software, even if
# Licensor is advised of the possibility of such damage.
#
# EXPORT REGULATION: Licensee agrees to comply with any and all applicable
# export control laws, regulations, and/or other laws related to embargoes and
# sanction programs administered by law.
#
# SEVERABILITY: If any provision(s) of this Agreement shall be held to be
# invalid, illegal, or unenforceable by a court or other tribunal of competent
# jurisdiction, the validity, legality and enforceability of the remaining
# provisions shall not in any way be affected or impaired thereby.
#
# NO IMPLIED WAIVERS: No failure or delay by Licensor in enforcing any right
# or remedy under this Agreement shall be construed as a waiver of any future
# or other exercise of such right or remedy by Licensor.
#
# GOVERNING LAW: This Agreement shall be construed and enforced in accordance
# with the laws of Germany without reference to conflict of laws principles.
# You consent to the personal jurisdiction of the courts of this country and
# waive their rights to venue outside of Germany.
#
# ENTIRE AGREEMENT AND AMENDMENTS: This Agreement constitutes the sole and
# entire agreement between Licensee and Licensor as to the matter set forth
# herein and supersedes any previous agreements, understandings, and
# arrangements between the parties relating hereto.
#
# THIS HEADER MAY NOT BE EXTRACTED OR MODIFIED IN ANY WAY.
##


import requests
import re
import gzip
from urlparse import urlparse

class TrafficSimulator(object):
	"""A simulator loads network traces and feeds them to the net2vec model

	Attributes:
	name:  the name (ID) of the model. Unique to each instance.        
	"""

	def __init__(self, name):
		"""Initialize the simulator."""
		self.name = name

	def load(self, path):
		"""Loads the trace file."""

	def run(self, numSteps=100):
		"""Run the simulation for extactly 100 steps."""

	def reset(self):
		"""Reset the simulation to the beginning of the trace."""


class TMSSimulator(TrafficSimulator):
	"""A simulator for TMS http request traces."""

	def __init__(self, name):
		"""Initialize the simulator."""
		self.name = name
		self.fh = None

		self.tracker_list = ['doubleclick.net', 'crwdcntrl.net', 'gstatic.com', 'adnxs.com', 'googlesyndication.com', 
                'mobads.baidu.com', 'darchermedia.com', 'admob.com', 'vpon.com', 'exoclick.com', 'snyu.com',
                'doublemax.net', 'twrank.com', 'focas.jp', 'localhost', 'pos.baidu.com', 'ad.tagtoo.co', 
                'ads-by.madadsmedia.com', 'g5t518j.net', 'socdm.com', 'i.yimg.jp', 'g.doubleclick.net:80',
                'edgesuite.net', 'scorecardresearch.com', 'tr.adsplats.com', 'pixnet.net', 'globaltraffico.com',
                'fetchback.com', 'taobaocdn.com', 'sstatic.net', 'xn--jgru0h7vjnpg9zj3yn.com']

	def load(self, path, is_gzip=True):
		"""Loads the trace file."""

		if is_gzip:
			self.fh = gzip.open(path)
		else:
			self.fh = open(path, "r")

	def run(self, numSteps=10, pipeline=1):
		"""Runs the simulation for a fixed number of steps. sends the tuples to net2vec pipeline. """

		# the pattern for an IP address (IPv4)
		ip_pattern = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")

		file_content = []
		# load the file content
		for i in range(numSteps):
			file_content.append(self.fh.readline())


		# iterate throught the lines of the file
		for line in file_content:

			# split up the line
			split_line = line.split(',')
			# the client ip of the request (TMS standard)
			ip = split_line[20]

			# extract the domain from the http request field        
			http_request = split_line[-1]
			http_request = http_request.split(' ')
			if len(http_request) != 3:
				continue
			# get the URL of the request
			request_url = http_request[1]

			# extract components of the URI
			scheme, domain, path, params, query, fragment = urlparse(request_url)

			# if we have no domain value, continue
			if len(domain) <= 0:
				continue

			# split the domain into its components
			domain_split = domain.split('.')
			# we have no . in the domainname --> bad format
			if len(domain_split) <= 1:
				continue               
			# we have exactly one "." (e.g., google.com, ...)
			if len(domain_split) == 2:
				domain = '.'.join(domain_split[-2:])
			# we have at least two "."
			if len(domain_split) >= 3:
				# check if the domain does not match an IP address
				if not ip_pattern.match(domain):
			    	# shorten the domain name to have only 2 "."
				    # e.g. bla3.bla2.bla1.com to bla2.bla1.com
				    domain = '.'.join(domain_split[-3:])

			# filter out some known trackers
			is_tracker = False
			for tr in self.tracker_list:
				if domain[-len(tr):] == tr:
				    is_tracker = True
				    break
			if is_tracker:
				continue

			# call the API with (ip, domain)		
			# print([str(ip), str(domain)])
			r = requests.put('http://localhost:4500/tuple', json={"pipeline": pipeline, "data": [str(ip), str(domain)]})


