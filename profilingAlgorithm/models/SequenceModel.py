##
# User Profiling System
#
# File:     SequenceModel.py
# Authors:  <Roberto Gonzalez (roberto.gonzalez@neclab.eu)>
# <Mathias Niepert (mathias.niepert@neclab.eu)>
# <David Friede (david.friede@neclab.eu)>
#
# NEC Laboratories Europe GmbH, Copyright (c) <year>, All rights reserved.
#
# THIS HEADER MAY NOT BE EXTRACTED OR MODIFIED IN ANY WAY.
#
# PROPRIETARY INFORMATION ---
#
# SOFTWARE LICENSE AGREEMENT
#
# ACADEMIC OR NON-PROFIT ORGANIZATION NONCOMMERCIAL RESEARCH USE ONLY
#
# BY USING OR DOWNLOADING THE SOFTWARE, YOU ARE AGREEING TO THE TERMS OF THIS
# LICENSE AGREEMENT.  IF YOU DO NOT AGREE WITH THESE TERMS, YOU MAY NOT USE OR
# DOWNLOAD THE SOFTWARE.
#
# This is a license agreement ("Agreement") between your academic institution
# or non-profit organization or self (called "Licensee" or "You" in this
# Agreement) and NEC Laboratories Europe GmbH (called "Licensor" in this
# Agreement).  All rights not specifically granted to you in this Agreement
# are reserved for Licensor.
#
# RESERVATION OF OWNERSHIP AND GRANT OF LICENSE: Licensor retains exclusive
# ownership of any copy of the Software (as defined below) licensed under this
# Agreement and hereby grants to Licensee a personal, non-exclusive,
# non-transferable license to use the Software for noncommercial research
#     purposes, without the right to sublicense, pursuant to the terms and
# conditions of this Agreement. NO EXPRESS OR IMPLIED LICENSES TO ANY OF
# LICENSOR'S PATENT RIGHTS ARE GRANTED BY THIS LICENSE. As used in this
# Agreement, the term "Software" means (i) the actual copy of all or any
# portion of code for program routines made accessible to Licensee by Licensor
# pursuant to this Agreement, inclusive of backups, updates, and/or merged
# copies permitted hereunder or subsequently supplied by Licensor,  including
# all or any file structures, programming instructions, user interfaces and
# screen formats and sequences as well as any and all documentation and
# instructions related to it, and (ii) all or any derivatives and/or
# modifications created or made by You to any of the items specified in (i).
#
# CONFIDENTIALITY/PUBLICATIONS: Licensee acknowledges that the Software is
# proprietary to Licensor, and as such, Licensee agrees to receive all such
# materials and to use the Software only in accordance with the terms of this
# Agreement.  Licensee agrees to use reasonable effort to protect the Software
# from unauthorized use, reproduction, distribution, or publication. All
# publication materials mentioning features or use of this software must
# explicitly include an acknowledgement the software was developed by NEC
# Laboratories Europe GmbH.
#
# COPYRIGHT: The Software is owned by Licensor.
#
# PERMITTED USES:  The Software may be used for your own noncommercial
# internal research purposes. You understand and agree that Licensor is not
# obligated to implement any suggestions and/or feedback you might provide
# regarding the Software, but to the extent Licensor does so, you are not
# entitled to any compensation related thereto.
#
# DERIVATIVES: You may create derivatives of or make modifications to the
# Software, however, You agree that all and any such derivatives and
# modifications will be owned by Licensor and become a part of the Software
# licensed to You under this Agreement.  You may only use such derivatives and
# modifications for your own noncommercial internal research purposes, and you
# may not otherwise use, distribute or copy such derivatives and modifications
# in violation of this Agreement.
#
# BACKUPS:  If Licensee is an organization, it may make that number of copies
# of the Software necessary for internal noncommercial use at a single site
# within its organization provided that all information appearing in or on the
# original labels, including the copyright and trademark notices are copied
# onto the labels of the copies.
#
# USES NOT PERMITTED:  You may not distribute, copy or use the Software except
# as explicitly permitted herein. Licensee has not been granted any trademark
# license as part of this Agreement.  Neither the name of NEC Laboratories
# Europe GmbH nor the names of its contributors may be used to endorse or
# promote products derived from this Software without specific prior written
# permission.
#
# You may not sell, rent, lease, sublicense, lend, time-share or transfer, in
# whole or in part, or provide third parties access to prior or present
# versions (or any parts thereof) of the Software.
#
# ASSIGNMENT: You may not assign this Agreement or your rights hereunder
# without the prior written consent of Licensor. Any attempted assignment
# without such consent shall be null and void.
#
# TERM: The term of the license granted by this Agreement is from Licensee's
# acceptance of this Agreement by downloading the Software or by using the
# Software until terminated as provided below.
#
# The Agreement automatically terminates without notice if you fail to comply
# with any provision of this Agreement.  Licensee may terminate this Agreement
# by ceasing using the Software.  Upon any termination of this Agreement,
# Licensee will delete any and all copies of the Software. You agree that all
# provisions which operate to protect the proprietary rights of Licensor shall
# remain in force should breach occur and that the obligation of
# confidentiality described in this Agreement is binding in perpetuity and, as
# such, survives the term of the Agreement.
#
# FEE: Provided Licensee abides completely by the terms and conditions of this
# Agreement, there is no fee due to Licensor for Licensee's use of the
#     Software in accordance with this Agreement.
#
# DISCLAIMER OF WARRANTIES:  THE SOFTWARE IS PROVIDED "AS-IS" WITHOUT WARRANTY
# OF ANY KIND INCLUDING ANY WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR
# FITNESS FOR A PARTICULAR USE OR PURPOSE OR OF NON- INFRINGEMENT.  LICENSEE
# BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF THE SOFTWARE AND
# RELATED MATERIALS.
#
# SUPPORT AND MAINTENANCE: No Software support or training by the Licensor is
# provided as part of this Agreement.
#
# EXCLUSIVE REMEDY AND LIMITATION OF LIABILITY: To the maximum extent
# permitted under applicable law, Licensor shall not be liable for direct,
#     indirect, special, incidental, or consequential damages or lost profits
# related to Licensee's use of and/or inability to use the Software, even if
# Licensor is advised of the possibility of such damage.
#
# EXPORT REGULATION: Licensee agrees to comply with any and all applicable
# export control laws, regulations, and/or other laws related to embargoes and
# sanction programs administered by law.
#
# SEVERABILITY: If any provision(s) of this Agreement shall be held to be
# invalid, illegal, or unenforceable by a court or other tribunal of competent
# jurisdiction, the validity, legality and enforceability of the remaining
# provisions shall not in any way be affected or impaired thereby.
#
# NO IMPLIED WAIVERS: No failure or delay by Licensor in enforcing any right
# or remedy under this Agreement shall be construed as a waiver of any future
# or other exercise of such right or remedy by Licensor.
#
# GOVERNING LAW: This Agreement shall be construed and enforced in accordance
# with the laws of Germany without reference to conflict of laws principles.
# You consent to the personal jurisdiction of the courts of this country and
# waive their rights to venue outside of Germany.
#
# ENTIRE AGREEMENT AND AMENDMENTS: This Agreement constitutes the sole and
# entire agreement between Licensee and Licensor as to the matter set forth
# herein and supersedes any previous agreements, understandings, and
# arrangements between the parties relating hereto.
#
# THIS HEADER MAY NOT BE EXTRACTED OR MODIFIED IN ANY WAY.


from collections import OrderedDict
import numpy as np
from gensim.models import Word2Vec
from cachetools import LRUCache


class SequenceModel(object):
    """A SeqModel is a machine learning model that takes a sequence of tuples
        and returns something

    Attributes:
        model: the actual machine learning model that has been trained (or should be trained)
        name:  the name (ID) of the model. Unique to each instance.        
    """

    def __init__(self, name, infer=True):
        """Return a SeqModel object whose name is *name* and which is either used for training or inference."""
        self.name = name
        self.infer = infer

    def load(self, path):
        """Loads a pretrained model from a file."""

    def train(self, xdata, ydata):
        """Trains the model from scratch. Set params in separate method."""

    def update(self, tseq, target):
        """Update the model (only possible if online SGD training possible)."""

    def update_batch(self, xdata, ydata):
        """Update the model with a batch of sequences."""

    def predict(self, seqData):
        """Apply the predict method (or equivalent) of the model and return directly"""

    def get_predict(self):
        """Return the value of the last predict call."""

    def predict_probab(self, seqData):
        """Apply the predict_probab method (or equivalent) of the model and return directly"""

    def get_predict_probab(self):
        """Return the value of the last predict_probab call."""

    def set_params(self, params):
        """Set the parameters of the internal machine learning model."""


class SeqModelGensimWord2Vec(SequenceModel):
    """A wrappter for the gensim model"""


    def __init__(self, name, infer=True, domainToCategories=None):
        """Return a SeqModel object whose name is *name* and which is 
        either used for training or inference."""
        # attributes of every model instance

        SequenceModel.__init__(self, name, infer)
        self.name = name
        self.infer = infer

        # a cache for requests
        self.cache = LRUCache(maxsize=10)
        # variable storing the most recently seen sequence
        self.most_recent_sequence = None

        # the actual machine learning model (here: gensim word2vec)
        self.model = None

        # mapping from domain names to categories
        self.domainToCategories = domainToCategories

        # parameters of the word2vec model (see gensim documentation)
        self.embedding_size = 100
        self.window_size = 5
        self.min_count = 5
        self.workers = 4
        # number of neighbors considered for the category assignment
        self.neighbor_count = 1000


    def load(self, path):
        """Loads a pretrained model from a file."""

        self.model = Word2Vec.load("%s/%s" %(path, self.name))

        if self.infer:
                self.model.init_sims(replace=True)


    def save(self, path):
        self.model.save("%s/%s" %(path, self.name))


    def train(self, xdata, ydata):
        """Trains the model from scratch. Set params in separate method."""

        # we can only train if the model is not used for inference
        if self.infer:
                return False

        self.model = Word2Vec(xdata, size=self.embedding_size, window=self.window_size,
                                min_count=self.min_count, workers=self.workers)

        return True

    def update(self, tseq, target):
        """Updates the underlying model with the given ((tseq), target) pair."""

        # we can only train if the model is not used for inference
        if self.infer:
                return False

        if self.model is not None:
                model.train([tseq])
                return True

        return False

    def updateBatch(self, xdata, ydata):
        """Update the model with a batch of sequences."""

        # we can only train if the model is not used for inference
        if self.infer:
                return False

        if self.model is not None:
                self.model.train(xdata, total_examples=self.model.corpus_count, epochs=self.model.iter)
                return True
        else:
            self.train(xdata, ydata)

        return False

    def predict(self, seqData):
        """Apply the predict method (or equivalent) of the model.
           Returns the most probable category for the given query sequence. 
        """
        # call predict_probab and fetch the category with the highest likelihood
        categoryToProb = self.predict_probab(seqData)

	if categoryToProb is not None:
		return categoryToProb.items()[0][0]
	else:
		return None


    def get_predict(self):
        """Return the last value of the predict function.
           Returns the most probable category for the given query sequence. 
        """

        if self.most_recent_sequence is not None:
                return self.cache[self.most_recent_sequence].items()[0][0]

        return False

    def predict_probab(self, seqData, way = "Sum", normalizePrediction = None, forExperiment = False):
        """
        :param seqData: a sequence of domain number visited by the user
        :return: a percentage of category per profile
        """

        if self.domainToCategories is None:
            return None

        results = np.zeros((len(self.domainToCategories[next(iter(self.domainToCategories))]),))
        results_exp = np.zeros((len(self.domainToCategories[next(iter(self.domainToCategories))]),))
        # make sure the model exists
        if self.model is None:
            return results


        #remove all elements that are not in the word2vec vocabulary
        includedDomains = [str(x) for x in seqData if str(x) in self.model.wv.vocab]

        # return false if includedDomains is empty
        if len(includedDomains) == 0:
                return None

        # we query for sets (word2vec is permutation invariant)
        cacheQuery = frozenset(includedDomains)
        # set the most recently used sequence
        self.most_recent_sequence = cacheQuery
        # check whether we have that request in the cache
        if cacheQuery in self.cache:
            return self.cache[cacheQuery]

        # most_sim = self.model.wv.most_similar(positive=includedDomains, topn=self.neighbor_count)

        for elem in includedDomains:
            if int(elem )in self.domainToCategories:
                # print "Elem in category: ", elem
                if way == "Sum":
                    results += self.domainToCategories[int(elem)]
                    results_exp += self.domainToCategories[int(elem)]
                elif way == "Max":
                    results = np.maximum(results, self.domainToCategories[int(elem)])
                    results_exp = np.maximum(results_exp, self.domainToCategories[int(elem)])
        if len(includedDomains) > 0:
            most_similar = self.model.most_similar(positive=includedDomains, topn=self.neighbor_count)
            for (elem, weight) in most_similar:
                #         print (elem, weight)
                if int(elem) in self.domainToCategories:
                    if way == "Sum":
                        results += weight * np.asarray(self.domainToCategories[int(elem)])
                    elif way == "Max":
                        results = np.maximum(results, weight * self.domainToCategories[int(elem)])

            if normalizePrediction == "Norm":
                from sklearn.preprocessing import normalize
                results = normalize(results).tolist()
                results_exp = normalize(results_exp).tolist()
            elif normalizePrediction == "Max1":
                results = results / max(results)
                results_exp = results_exp / max(results_exp)
            if forExperiment:
                self.cache[cacheQuery] = (results, results_exp)
            else:
                self.cache[cacheQuery] = results

        if forExperiment:
            return (results, results_exp)
        else:
            return results




    # def predict_probab(self, seqData):
    #     """Apply the predict method (or equivalent) of the model.
    #        Returns a dictionary with categories and ther probabilities for the given query.
    #     """
    #
    #     # make sure the model exists
    #     if self.model is None:
    #             return None
    #
    #     # the list of included domains/urls
    #     includedDomains = []
    #     #remove all elements that are not in the word2vec vocabulary
    #     for dm in seqData:
    #             if dm in self.model.vocab:
    #                     includedDomains.append(dm)
    #
    #     # return false if includedDomains is empty
    #     if len(includedDomains) == 0:
    #             return None
    #
    #     # we query for sets (word2vec is permutation invariant)
    #     cacheQuery = frozenset(includedDomains)
    #     # set the most recently used sequence
    #     self.most_recent_sequence = cacheQuery
    #     # check whether we have that request in the cache
    #     if cacheQuery in self.cache:
    #         return self.cache[cacheQuery]
    #
    #     # call the model to get the top x most similar domains
    #     most_sim = self.model.most_similar(positive=includedDomains, topn=self.neighbor_count)
    #
    #     # append the domain in the query themselves
    #     for dm in includedDomains:
    #         most_sim.append( (dm, 1.0) )
    #
    #     categoryToProb = dict()
    #     # iterate through all the most similar domain names
    #     # domainList is of form  [domainName, similarity]
    #     for domainList in most_sim:
    #         domainName = domainList[0]
    #         if domainName in self.domainToCategories:
    #             for currentCategory in self.domainToCategories[domainName]:
    #                 if currentCategory in categoryToProb:
    #                     # take the max similarity
    #                     categoryToProb[currentCategory] = max(categoryToProb[currentCategory], domainList[1])
    #                 else:
    #                     categoryToProb[currentCategory] = domainList[1]
    #
    #
    #     # sort the dictionary and return an ordered dictionary
    #     categoryToProb = OrderedDict(sorted(categoryToProb.items(), key=lambda t: t[1], reverse=True))
    #     self.cache[cacheQuery] = categoryToProb

	# if len(categoryToProb) > 0:
	# 	return categoryToProb
	# else:
	# 	return None


    def get_predict_probab(self):
        """Return the last value of the predict_probab function.
           Returns the most probable category for the given query sequence. 
        """

        if self.most_recent_sequence is not None:
                return self.cache[most_recent_sequence]

        return False
        
    def set_domainToCategories(self, domainToCategories):
        # mapping from domain names to categories
        self.domainToCategories = domainToCategories

